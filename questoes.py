def index():
    print("Selecione umas das opções abaixo:\n 1- Analise de ananograma de palavras \n 2- Listagem ordenada de numeros")
    questao = int(input(": "))

    if (questao == 1):
        palavra1 = input("Digite uma palavra: ")
        palavra2 = input("Digite a segunda palavra: ")
        
        anagrama(palavra1, palavra2)
    elif(questao == 2) :
        quantidade = int(input("Quantos numero deseja inserir? "))
        numerosVetor = []

        for contador in range(quantidade):
            numero = int(input("informe um numero: "))
            numerosVetor.append(numero)
        
        listaOrdenada(numerosVetor)

    else :
        print("opção invalida..")

def anagrama(palavra1 , palavra2):
    # Analise de caracter
    if(len(palavra1) != len(palavra2)) :
        print(False)
    else:
        # retirando letras maiusculas
        palavra1 = palavra1.lower()
        palavra2 = palavra2.lower()
        
        anagrama = True

        for letra in palavra1 :
            if(letra not in palavra2) :
                anagrama = False
                break
        
        print(anagrama)

def listaOrdenada (numeros):
    numerosImpares = []
    numerosPares = []
    
    for numero in numeros:
        if(numero % 2 == 0) :
            numerosPares.append(numero)
        else:
            numerosImpares.append(numero)
    
    # Ordenação de numeros
    ordernarNumerosImpares = ordernarNumeros(numerosImpares, "crescente")
    ordernarNumerosPares = ordernarNumeros(numerosPares, "descrescente")

    print(ordernarNumerosPares + ordernarNumerosImpares)

def ordernarNumeros(numeros, tipo):
    numeroOrdenados = []
    ordenado = False

    if (tipo == "crescente") :
        while ordenado == False:
            posicao = 0

            if(numeros != []):
                menor = min(numeros)
                numeroOrdenados.append(menor)

                for numero in numeros:
                    if(menor == numero):
                        del numeros[int(posicao)]
                    posicao += 1              
            else :
                ordenado = True
        
        return numeroOrdenados
    else :
        while ordenado == False:
            posicao = 0

            if(numeros != []):
                maior = max(numeros)
                numeroOrdenados.append(maior)

                for numero in numeros:
                    if(maior == numero):
                        del numeros[int(posicao)]
                    posicao += 1              
            else :
                ordenado = True
        
        return numeroOrdenados

# Inicio do programa        
index()