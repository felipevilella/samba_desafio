from flask import Flask,jsonify, request
app = Flask(__name__)

videos = []

@app.route('/medias',methods=['POST'])
def upload():
    idVideo = int(request.form['id'])
    nome = request.form['nome']
    url = request.form['url']
    duracao = int(request.form['duracao'])

    # Validação de nome
    if(nome == "") :
        return jsonify({'error': 'Campo nome é obrigatorio'}), 400
    elif(len(nome) > 512) :
        return jsonify({'error': 'Campo nome é até 512 caracteres'}), 400

    # Validação de id
    if(idVideo == "") : 
          return jsonify({'error': 'Campo id é obrigatorio'}), 400
    elif(idVideo >= 10000000) :
           return jsonify({'error': 'Campo id é até 10000000'}), 400
    else:
        for video in videos:
            if(video['id'] == idVideo):
                return jsonify({'error': 'Não foi possivel salvar informações do video, id existente!'}), 400
    
    # Validação de url
    if(len(url) > 512) :
           return jsonify({'error': 'Campo url é até 512 caracter'}), 400

    # validação de tempo de duração
    if(duracao < 0 or duracao > 10000000) :
           return jsonify({'error': 'Campo duração é de 0 até 10000000'}), 400

    dados = {
                'id': idVideo,
                'nome': nome, 
                'url':  url, 
                'duracao': str(duracao)
            }

    # Salvar dados
    videos.append(dados)
    
    return jsonify(dados), 200

@app.route('/medias',methods=['GET'])
def listar():
    return jsonify(videos), 200

@app.route('/medias/<int:id>', methods=['GET'])
def buscar(id):
    for video in videos:
        if(video['id'] == id):
            return jsonify(video), 200
                
    return jsonify({'error': 'Video não encontrado'}), 404

if __name__ == '__main__':
    app.run(debug=True)