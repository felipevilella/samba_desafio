Instruções:
**********************
 - Para rodar as questões iniciais, siga os passos abaixo:
	- Caso não tenha o python instalado no seu pc, baixei atraves do https://www.python.org/downloads/
	- Abre o prompt de comando e entre no diretorio do arquivo
	- Digite python questoes.py
	- Selecione a opção das atividades para realizar o teste

 - Para rodar a API, siga as instruções abaixo:
	- Acesse desafio_sambaTech\API atraves do prompt
	- Digite python python video.py
	- Copie o ip com a porta gerado pela aplicado, geralmente é http://127.0.0.1:5000/
	- Acesse a documentação https://documenter.getpostman.com/view/6256908/SzKVRJL3?version=latest
	- Abra o Postaman e teste a API